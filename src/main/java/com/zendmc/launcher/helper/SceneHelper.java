package com.zendmc.launcher.helper;

import com.zendmc.launcher.app.App;
import com.zendmc.launcher.controller.AbstractController;
import com.zendmc.launcher.util.Utf8Control;
import javafx.animation.FadeTransition;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SceneHelper {
    private static HashMap<String, Scene> scenes = new HashMap<>();

    public static void reload() {
        scenes = new HashMap<>();
    }

    public static void reload(String name) {
        scenes.remove(name);
    }

    public static Scene getScene(String name) {
        return getScene(name, false);
    }

    public static Scene getScene(String name, boolean reload) {
        if (!scenes.containsKey(name) || reload) {
            final FXMLLoader loader = new FXMLLoader();
            loader.setLocation(ClassLoader.getSystemResource("fxml/" + name + ".fxml"));
            loader.setResources(ResourceBundle.getBundle(
                    String.format("lang/%s/messages", name),
                    new Locale(App.SETTINGS.getJSONObject("launcher").getString("language")),
                    new Utf8Control()
            ));

            try {
                Scene scene = new Scene(loader.load());
                scenes.put(name, scene);
                ((AbstractController) loader.getController()).setScene(scene);
            } catch (RuntimeException | IOException x) {
                Logger.getGlobal().log(Level.SEVERE, null, x);
            }
        }

        return scenes.get(name);
    }

    public static void switchAnimated(Stage stage, String sceneFromName, String sceneToName) {
        Scene sceneFrom = getScene(sceneFromName);
        Scene sceneTo = getScene(sceneToName);
        fade(false, sceneFrom.getRoot());
        stage.setScene(sceneTo);
        fade(true, sceneTo.getRoot());
    }

    private static void fade(boolean in, Parent root) {
        FadeTransition ftCurrent = new FadeTransition(Duration.millis(100), root);
        ftCurrent.setFromValue(in ? 0.5 : 1.0);
        ftCurrent.setToValue(in ? 1.0 : 0.5);
        ftCurrent.play();
    }
}
