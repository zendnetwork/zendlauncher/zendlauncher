package com.zendmc.launcher.helper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

public class JsonHelper {
    public static JSONObject merge(JSONObject main, JSONObject update) {
        for (String key : update.keySet()) {
            Object object = main.opt(key);
            if (object instanceof JSONArray) {
                update.getJSONArray(key).forEach(it -> main.getJSONArray(key).put(it));
            } else if (object instanceof JSONObject) {
                main.put(key, merge(main.getJSONObject(key), update.getJSONObject(key)));
            } else {
                main.put(key, update.get(key));
            }
        }

        return main;
    }

    public static JSONObject remove(JSONObject main, JSONObject update) {
        for (String key : update.keySet()) {
            Object object = update.opt(key);
            if (object instanceof JSONArray) {
                update.getJSONArray(key).forEach(it -> {
                    List x = main.getJSONArray(key).toList();
                    x.remove(it);
                    main.put(key, new JSONArray(x));
                });
            } else if (object instanceof JSONObject) {
                main.put(key, remove(main.getJSONObject(key), update.getJSONObject(key)));
            } else {
                main.remove(key);
            }
        }

        return main;
    }
}
