package com.zendmc.launcher.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FileUtils {
    public static void createFile(Path path, boolean replace) throws IOException {
        if (!path.toFile().exists() || replace) {
            if (replace) {
                Files.deleteIfExists(path);
            }
            Files.createDirectories(path.getParent());
            Files.createFile(path);
            Logger.getGlobal().info(String.format("Created new file at %s", path));
        }
    }

    public static Path createTempFile(String suffix) throws IOException {
        Path path = Files.createTempFile("ZendLauncher-", suffix);
        path.toFile().deleteOnExit();
        return path;
    }

    public static void unpackJarFile(String destinationDir, String jarPath) {
        try(JarFile jar = new JarFile(new File(jarPath))) {
            // Now create all files
            for (Enumeration<JarEntry> enums = jar.entries(); enums.hasMoreElements(); ) {
                JarEntry entry = enums.nextElement();

                String fileName = destinationDir + File.separator + entry.getName();

                File f = new File(fileName);
                if (fileName.contains("META") || fileName.contains(".git") || fileName.contains(".sha1") || f.exists()) {
                    continue;
                }
                Files.createDirectories(f.toPath().getParent());

                try (ReadableByteChannel rbc = Channels.newChannel(jar.getInputStream(entry));
                     FileOutputStream fos = new FileOutputStream(f)) {
                    fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
                }
            }

        } catch (IOException ex) {
            Logger.getGlobal().log(Level.SEVERE, null, ex);
        }
    }
}