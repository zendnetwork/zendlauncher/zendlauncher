package com.zendmc.launcher.util;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DialogPane;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class AlertUtils {
    private static Image cachedImage;

    private static Image getCachedImage() {
        if (cachedImage == null) {
            cachedImage = new Image(
                    ClassLoader.getSystemResourceAsStream("assets/icon.png")
            );
        }

        return cachedImage;
    }

    public static boolean simpleAlert(Alert.AlertType alertType, String header, String content) {
        Alert alert = new Alert(alertType);
        alert.setTitle("ZendLauncher");
        alert.setHeaderText(header);
        alert.setContentText(content);
        DialogPane dialogPane = alert.getDialogPane();
        dialogPane.getStylesheets().add(ClassLoader.getSystemResource("css/alert.css").toExternalForm());

        ((Stage) dialogPane.getScene().getWindow()).getIcons().add(getCachedImage());

        alert.showAndWait();

        return alert.getResult() == ButtonType.OK || alert.getResult() == ButtonType.YES;
    }
}
