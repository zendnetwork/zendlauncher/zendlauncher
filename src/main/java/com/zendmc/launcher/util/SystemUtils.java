package com.zendmc.launcher.util;

import com.zendmc.launcher.app.AppData;

import javax.xml.bind.DatatypeConverter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


public class SystemUtils {
    private static HashMap<Class, String> jarHashes;
    private static String OSName;
    private static String OSArch;
    private static String javaExecutable;
    private static String defaultGameDirectory;

    public static String getJarHash(Class clazz) throws IOException, NoSuchAlgorithmException {
        if (jarHashes == null) {
            jarHashes = new HashMap<>();
        }

        if (!jarHashes.containsKey(clazz)) {
            File currentJarFile = new File(clazz.getProtectionDomain().getCodeSource().getLocation().getPath());
            byte[] b = Files.readAllBytes(Paths.get(currentJarFile.toString()));
            byte[] hash = MessageDigest.getInstance("MD5").digest(b);
            jarHashes.put(clazz, DatatypeConverter.printHexBinary(hash));
        }

        return jarHashes.get(clazz);
    }

    public static String getOSName() {
        if (OSName == null) {
            String OS = System.getProperty("os.name").toLowerCase();

            if (OS.contains("win")) {
                OSName = "windows";
            } else if (OS.contains("mac")) {
                OSName = "macos";
            } else if (OS.contains("nix") || OS.contains("nux") || OS.contains("aix")) {
                OSName = "linux";
            } else {
                Logger.getGlobal().log(Level.SEVERE, "Unsupported OS!");
                System.exit(-1);
            }
        }

        return OSName;
    }

    public static String getOSArch() {
        if (OSArch == null) {
            String OS = System.getProperty("os.arch");

            if (OS.contains("86")) {
                OSArch = "32";
            } else if (OS.contains("64")) {
                OSArch = "64";
            } else {
                Logger.getGlobal().log(Level.SEVERE, "Unsupported OS!");
                System.exit(-1);
            }
        }

        return OSArch;
    }

    public static String getJavaExecutable() {
        if (javaExecutable == null) {
            javaExecutable = System.getProperty("java.home") + "/bin/java" + (getOSName().equals("windows") ? "w.exe" : "");
        }

        return javaExecutable;
    }

    public static String getDefaultGameDirectory() {
        if (defaultGameDirectory == null) {
            defaultGameDirectory = AppData.APP_DIRECTORY
                    .replace("ZendLauncher", "Minecraft")
                    .replace(".zendlauncher", ".minecraft");
        }

        return defaultGameDirectory;
    }
}
