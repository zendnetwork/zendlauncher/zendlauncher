package com.zendmc.launcher.util;

import com.zendmc.launcher.app.AppData;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HttpUtils {
    private static ThreadPoolExecutor threadPool;

    public static ThreadPoolExecutor getNewThreadPool() {
        return new ThreadPoolExecutor(
                10,
                20,
                0,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(3000)
        );
    }

    public static ThreadPoolExecutor getThreadPool() {
        if (threadPool == null) {
            threadPool = getNewThreadPool();
        }

        return threadPool;
    }

    public static void setThreadPool(ThreadPoolExecutor newThreadPool) {
        threadPool = newThreadPool;
    }

    public static boolean isInternetConnected() {
        try {
            return InetAddress
                    .getByName("www.google.com")
                    .isReachable(3000);
        } catch (IOException ignored) {
        }
        return false;
    }

    public static byte[] downloadFile(String url, Path path, boolean replace, boolean load, boolean async) {
        Callable<byte[]> lambda = () -> {
            FileUtils.createFile(path, replace);

            if (path.toFile().length() == 0 || replace) {
                HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
                connection.setDoOutput(true);
                connection.setRequestProperty("User-Agent", AppData.USER_AGENT);

                try (ReadableByteChannel readableByteChannel = Channels.newChannel(connection.getInputStream());
                     FileOutputStream fileOutputStream = new FileOutputStream(path.toFile())) {
                    fileOutputStream.getChannel().transferFrom(readableByteChannel, 0, Long.MAX_VALUE);
                    Logger.getGlobal().info(String.format("Downloaded file from %s to %s", url, path));
                }
                connection.disconnect();
            }

            return load ? Files.readAllBytes(path) : null;
        };

        try {
            if (async) {
                Future<byte[]> submit = getThreadPool().submit(lambda);
                return load ? submit.get() : null;
            }

            return lambda.call();
        } catch (Exception ex) {
            Logger.getGlobal().log(
                    Level.SEVERE,
                    String.format("Unknown error during downloading file from %s to %s", url, path),
                    ex
            );
        }

        return null;
    }

    public static JSONObject downloadJsonObject(String url, Path path, boolean replace, boolean load) {
        return new JSONObject(new String(Objects.requireNonNull(downloadFile(url, path, replace, load, false))));
    }

    public static JSONArray downloadJsonArray(String url, Path path, boolean replace, boolean load) {
        return new JSONArray(new String(Objects.requireNonNull(downloadFile(url, path, replace, load, false))));
    }

    public static void downloadVoidAsync(String url, Path path, boolean replace) {
        downloadFile(url, path, replace, false, true);
    }
}
