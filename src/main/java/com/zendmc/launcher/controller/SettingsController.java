package com.zendmc.launcher.controller;

import com.zendmc.launcher.app.App;
import com.zendmc.launcher.app.AppData;
import com.zendmc.launcher.helper.SceneHelper;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.util.StringConverter;
import org.json.JSONObject;

import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;

public class SettingsController extends AbstractController {
    @FXML
    private Label versionLabel;
    @FXML
    private ComboBox<Locale> languageField;
    @FXML
    private CheckBox closeLauncherField;
    @FXML
    private CheckBox addZendToServers;
    @FXML
    private TextField gameDirectoryField;
    @FXML
    private Slider gameMemoryGBField;
    @FXML
    private TextField jvmArgumentsField;
    @FXML
    private CheckBox showCommunityField;
    @FXML
    private CheckBox showSnapshotsField;
    @FXML
    private CheckBox showLegacyField;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        versionLabel.setText(String.format(resources.getString("label.version"), AppData.APP_VERSION));

        App.SETTINGS.getJSONArray("locales").forEach(locale ->
                languageField.getItems().add(new Locale((String) locale))
        );
        languageField.setConverter(new StringConverter<Locale>() {
            @Override
            public String toString(Locale object) {
                String name = object.getDisplayLanguage(object);
                return name.substring(0, 1).toUpperCase() + name.substring(1);
            }

            @Override
            public Locale fromString(String string) {
                return languageField.getValue();
            }
        });

        JSONObject gameSettings = App.SETTINGS.getJSONObject("game");
        gameDirectoryField.setText(gameSettings.getString("directory"));
        gameMemoryGBField.setValue(gameSettings.getDouble("memory") / 1024);
        jvmArgumentsField.setText(gameSettings.getString("jvmArgs"));

        JSONObject launcherSettings = App.SETTINGS.getJSONObject("launcher");
        languageField.getSelectionModel().select(new Locale(launcherSettings.getString("language")));
        closeLauncherField.setSelected(launcherSettings.getBoolean("closeLauncher"));
        addZendToServers.setSelected(launcherSettings.getBoolean("addZendToServers"));

        JSONObject showSettings = launcherSettings.getJSONObject("show");
        showCommunityField.setSelected(showSettings.getBoolean("community"));
        showSnapshotsField.setSelected(showSettings.getBoolean("snapshots"));
        showLegacyField.setSelected(showSettings.getBoolean("legacy"));
    }

    @FXML
    private void save() {
        JSONObject gameSettings = App.SETTINGS.getJSONObject("game");
        gameSettings.put("directory", gameDirectoryField.getText());
        gameSettings.put("memory", (int) (gameMemoryGBField.getValue() * 1024));
        gameSettings.put("jvmArgs", jvmArgumentsField.getText());

        JSONObject launcherSettings = App.SETTINGS.getJSONObject("launcher");
        launcherSettings.put("language", languageField.getValue().getLanguage());
        launcherSettings.put("closeLauncher", closeLauncherField.isSelected());

        JSONObject showSettings = launcherSettings.getJSONObject("show");
        showSettings.put("community", showCommunityField.isSelected());
        showSettings.put("snapshots", showSnapshotsField.isSelected());
        showSettings.put("legacy", showLegacyField.isSelected());

        Platform.setImplicitExit(closeLauncherField.isSelected());
        SceneHelper.reload();
        SceneHelper.switchAnimated(App.STAGE, "settings", "main");
    }

    @FXML
    private void cancel() {
        SceneHelper.switchAnimated(App.STAGE, "settings", "main");
        SceneHelper.reload("settings");
    }
}
