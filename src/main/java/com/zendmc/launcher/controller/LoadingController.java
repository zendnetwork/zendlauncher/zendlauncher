package com.zendmc.launcher.controller;

import com.zendmc.launcher.event.LaunchEvent;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.net.URL;
import java.util.ResourceBundle;

public class LoadingController extends AbstractController {
    @FXML
    private Label status;

    private ResourceBundle resources;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.resources = resources;
        EventBus.getDefault().register(this);
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onDownloadEvent(LaunchEvent event) {
        Platform.runLater(() -> status.setText(resources.getString("status.download")));
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onLaunchEvent(LaunchEvent event) {
        Platform.runLater(() -> status.setText(resources.getString("status.launch")));
    }
}
