package com.zendmc.launcher.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;

public abstract class AbstractController implements Initializable {
    @FXML
    protected Parent root;

    protected Scene scene;

    public void setScene(Scene scene) {
        this.scene = scene;
    }
}
