package com.zendmc.launcher.controller;

import com.zendmc.launcher.app.App;
import com.zendmc.launcher.event.DownloadEvent;
import com.zendmc.launcher.event.LaunchEvent;
import com.zendmc.launcher.game.Authenticator;
import com.zendmc.launcher.game.Downloader;
import com.zendmc.launcher.helper.SceneHelper;
import com.zendmc.launcher.util.AlertUtils;
import com.zendmc.launcher.util.HttpUtils;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.util.StringConverter;
import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.UUID;
import java.util.concurrent.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class MainController extends AbstractController {
    @FXML
    private TextField usernameField;
    @FXML
    private PasswordField passwordField;
    @FXML
    private ComboBox<JSONObject> versionsListField;
    @FXML
    private CheckBox rememberMeField;

    private ResourceBundle resources;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.resources = resources;

        JSONObject accountSettings = App.SETTINGS.getJSONObject("account");
        JSONObject launcherSettings = App.SETTINGS.getJSONObject("launcher");

        versionsListField.getSelectionModel().selectedItemProperty().addListener((x, y, newValue) ->
                launcherSettings.put("selectedVersion", newValue.getString("type") + "-" + newValue.getString("id"))
        );

        root.setDisable(true);
        new Thread(() -> {
            JSONObject showSettings = App.SETTINGS.getJSONObject("launcher").getJSONObject("show");
            boolean community = showSettings.getBoolean("community");
            boolean snapshots = showSettings.getBoolean("snapshots");
            boolean legacy = showSettings.getBoolean("legacy");

            versionsListField.getItems().addAll(
                    Downloader.VERSIONS
                            .stream()
                            .filter(x -> !((!community && x.getString("vendor").equals("community"))
                                    || !snapshots && x.getString("type").equals("snapshot")
                                    || !legacy && x.getString("group").equals("classic"))
                            )
                            .collect(Collectors.toList())
            );

            Platform.runLater(() -> {
                String selectedVersion = launcherSettings.optString("selectedVersion");
                boolean selectFirst = selectedVersion.isEmpty();

                if (!selectFirst) {
                    for (JSONObject version : versionsListField.getItems()) {
                        if ((version.getString("type") + "-" + version.getString("id")).equals(selectedVersion)) {
                            versionsListField.getSelectionModel().select(version);
                            selectFirst = false;
                            break;
                        } else {
                            selectFirst = true;
                        }
                    }
                }

                if (selectFirst) {
                    versionsListField.getSelectionModel().selectFirst();
                }

                root.setDisable(false);
            });
        }).start();

        versionsListField.setConverter(new StringConverter<JSONObject>() {
            @Override
            public String toString(JSONObject object) {
                return String.format(
                        resources.getString("version." + object.getString("type")),
                        object.getString("id")
                );
            }

            @Override
            public JSONObject fromString(String string) {
                return versionsListField.getValue();
            }

        });

        boolean rememberMe = launcherSettings.getBoolean("rememberMe");
        rememberMeField.setSelected(rememberMe);
        rememberMeField.selectedProperty().addListener((x, y, newValue) ->
                launcherSettings.put("launcher", newValue)
        );

        usernameField.setText(accountSettings.getString("username"));
        if (rememberMe && accountSettings.getBoolean("sessionSaved")) {
            passwordField.setText("0000000000");
        }
    }

    @FXML
    private void play() {
        JSONObject accountSettings = App.SETTINGS.getJSONObject("account");

        root.setDisable(true);
        if (passwordField.getText().length() > 0 && HttpUtils.isInternetConnected()) {
            Logger.getGlobal().info("Authentication started");

            ExecutorService executor = Executors.newSingleThreadExecutor();
            Callable<Boolean> callable = () ->
                    Authenticator.authenticate(usernameField.getText(), passwordField.getText());
            Future<Boolean> future = executor.submit(callable);

            try {
                if (!future.get()) {
                    onFailedAuthentication();
                    Logger.getGlobal().info("Authentication failed");
                    root.setDisable(false);
                    return;
                }
            } catch (InterruptedException | ExecutionException ex) {
                Logger.getGlobal().log(Level.SEVERE, "Authentication failed due to the exception", ex);
                System.exit(-1);
            }

            executor.shutdownNow();
            Logger.getGlobal().info("Authentication succeed");
        } else {
            accountSettings.put("nickname", usernameField.getText());
            accountSettings.put("uuid", UUID.randomUUID().toString());
            accountSettings.put("accessToken", UUID.randomUUID().toString());
            accountSettings.put("sessionSaved", false);
        }
        root.setDisable(false);
        accountSettings.put("username", usernameField.getText());
        if (HttpUtils.isInternetConnected()) {
            EventBus.getDefault().post(new DownloadEvent(versionsListField.getValue(), true));
        } else {
            Logger.getGlobal().warning("No Internet! NOT CHECKING FILES!");
            EventBus.getDefault().post(new LaunchEvent(versionsListField.getValue()));
        }
        SceneHelper.switchAnimated(App.STAGE, "main", "loading");
    }

    @FXML
    private void openSettings() {
        SceneHelper.switchAnimated(App.STAGE, "main", "settings");
    }

    private void onFailedAuthentication() {
        usernameField.requestFocus();

        ChangeListener<? super String> r = (a, b, c) -> {
            usernameField.getStyleClass().remove("invalid-input");
            passwordField.getStyleClass().remove("invalid-input");
        };

        for (TextField field : new TextField[]{usernameField, passwordField}) {
            field.getStyleClass().add("invalid-input");
            field.textProperty().addListener(r);
        }

        AlertUtils.simpleAlert(
                Alert.AlertType.ERROR,
                resources.getString("error.wrongCredentials.header"),
                resources.getString("error.wrongCredentials.content")
        );
    }
}
