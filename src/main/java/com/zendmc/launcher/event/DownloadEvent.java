package com.zendmc.launcher.event;

import org.json.JSONObject;

public class DownloadEvent {
    public final JSONObject version;
    public final boolean launch;

    public DownloadEvent(JSONObject version, boolean launch) {
        this.version = version;
        this.launch = launch;
    }
}
