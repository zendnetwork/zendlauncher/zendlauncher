package com.zendmc.launcher.event;

import org.json.JSONObject;

public class LaunchEvent {
    public final JSONObject version;
    public LaunchEvent(JSONObject version) {
        this.version = version;
    }
}
