package com.zendmc.launcher.app;

import javafx.application.Preloader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class AppPreloader extends Preloader {
    public static Stage STAGE;

    @Override
    public void start(Stage primaryStage) {
        STAGE = primaryStage;
        ImageView iw = new ImageView(new Image(ClassLoader.getSystemResourceAsStream("assets/splash.png")));
        Pane pane = new Pane();
        pane.getChildren().add(iw);

        STAGE.setScene(new Scene(pane));
        STAGE.getIcons().add(new Image(ClassLoader.getSystemResourceAsStream("assets/icon.png")));
        STAGE.initStyle(StageStyle.UNDECORATED);
        STAGE.centerOnScreen();
        STAGE.show();
    }
}
