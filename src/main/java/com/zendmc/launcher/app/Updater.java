package com.zendmc.launcher.app;

import com.zendmc.launcher.util.AlertUtils;
import com.zendmc.launcher.util.HttpUtils;
import com.zendmc.launcher.util.SystemUtils;
import com.zendmc.launcher.util.Utf8Control;
import javafx.scene.control.Alert;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Updater {
    public static void checkUpdates() {
        try {
            JSONObject updates = HttpUtils.downloadJsonObject(
                    AppData.UPDATE_META_URL,
                    File.createTempFile("ZendLauncherUpdater-", "-meta").toPath(),
                    false,
                    true
            );

            String oldHash = SystemUtils.getJarHash(Main.class);
            if (!oldHash.equalsIgnoreCase(updates.getString("latest"))) {
                ResourceBundle bundle = ResourceBundle.getBundle(
                        String.format("lang/%s/messages", "updater"),
                        new Locale(App.SETTINGS.getJSONObject("launcher").getString("language")),
                        new Utf8Control()
                );

                if (AlertUtils.simpleAlert(
                        Alert.AlertType.CONFIRMATION,
                        bundle.getString("alert.updateAvailable.header"),
                        bundle.getString("alert.updateAvailable.content")
                )) update(oldHash, bundle);
            }
        } catch (Exception ex) {
            Logger.getGlobal().log(Level.SEVERE, null, ex);
        }
    }

    private static void update(String oldHash, ResourceBundle bundle) throws Exception {
        File currentJarFile = new File(Main.class.getProtectionDomain().getCodeSource().getLocation().getPath());
        File tempFile = File.createTempFile("ZendLauncherUpdater-", "-update");

        HttpUtils.downloadFile(
                AppData.UPDATE_FILE_URL,
                tempFile.toPath(),
                true,
                false,
                false
        );

        AlertUtils.simpleAlert(
                Alert.AlertType.INFORMATION,
                bundle.getString("alert.updated.header"),
                bundle.getString("alert.updated.content")
        );

        Files.copy(tempFile.toPath(), new FileOutputStream(currentJarFile));
        tempFile.delete();

        final ArrayList<String> command = new ArrayList<>();
        command.add(SystemUtils.getJavaExecutable());
        command.add("-jar");
        command.add(currentJarFile.getPath());
        command.add("--updated");
        command.add(oldHash);

        new ProcessBuilder(command).start();
        System.exit(0);
    }
}
