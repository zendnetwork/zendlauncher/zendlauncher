package com.zendmc.launcher.app;

import com.zendmc.launcher.util.SystemUtils;

import java.io.IOException;
import java.util.Objects;
import java.util.Properties;

public class AppData {
    public static final String APP_VERSION = getAppVersion();
    public static final String APP_DIRECTORY = getAppDirectory();
    public static final String USER_AGENT = "Mozilla/5.0 QuickLauncher/" + APP_VERSION;
    public static final String UPDATE_META_URL = "https://meta.zendmc.com/launcher/updates.json";
    public static final String UPDATE_FILE_URL = "https://download.zendmc.com/launcher/latest.jar";
    public static final String MIGRATION_URL = "https://meta.zendmc.com/launcher/migrations/%s.json";

    private static String getAppVersion() {
        Properties p = new Properties();
        try {
            p.load(ClassLoader.getSystemResourceAsStream("manifest.properties"));
        } catch (IOException e) {
            System.out.println("Init err 1");
            System.exit(-1);
        }
        return p.getProperty("version");
    }

    private static String getAppDirectory() {
        String path = System.getProperty("user.home") + "/.zendlauncher";
        if (Objects.equals(SystemUtils.getOSName(), "windows")) {
            String appData = System.getenv("APPDATA");
            return (appData == null) ? path :appData + "/ZendLauncher";
        }
        return path;
    }
}