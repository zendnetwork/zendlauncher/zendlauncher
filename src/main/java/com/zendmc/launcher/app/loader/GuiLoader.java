package com.zendmc.launcher.app.loader;

import com.zendmc.launcher.app.App;
import com.zendmc.launcher.app.AppPreloader;
import com.zendmc.launcher.helper.SceneHelper;
import javafx.application.Platform;
import javafx.scene.image.Image;

public class GuiLoader {
    public static void start() {
        App.STAGE.getIcons().add(new Image(ClassLoader.getSystemResourceAsStream("assets/icon.png")));
        App.STAGE.setResizable(false);
        App.STAGE.setTitle("Zend Launcher");
        App.STAGE.setOnCloseRequest(e -> Platform.exit());
        App.STAGE.setScene(SceneHelper.getScene("main"));
        AppPreloader.STAGE.hide();
        App.STAGE.show();
    }

    public static void stop() {
        App.STAGE.close();
    }
}
