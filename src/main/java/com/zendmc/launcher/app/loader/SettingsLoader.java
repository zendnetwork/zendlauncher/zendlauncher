package com.zendmc.launcher.app.loader;

import com.zendmc.launcher.app.App;
import com.zendmc.launcher.app.AppData;
import com.zendmc.launcher.util.FileUtils;
import com.zendmc.launcher.util.SystemUtils;
import org.json.JSONObject;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

public class SettingsLoader {
    public static void start() throws IOException {
        File file = new File(AppData.APP_DIRECTORY + "/settings.json");
        try (InputStream configFile = ClassLoader.getSystemResourceAsStream("settings.json")) {
            if (!file.exists() || file.length() == 0) {
                copy(configFile, file.toPath());
            }

            App.SETTINGS = new JSONObject(new String(Files.readAllBytes(file.toPath())));
        }
    }

    public static void stop() throws FileNotFoundException {
        try (PrintWriter out = new PrintWriter(new OutputStreamWriter(
                new FileOutputStream(AppData.APP_DIRECTORY + "/settings.json"),
                StandardCharsets.UTF_8
        ))) {
            out.println(App.SETTINGS);
        }
    }

    private static void copy(InputStream in, Path out) throws IOException {
        FileUtils.createFile(out, false);
        Files.copy(in, out, StandardCopyOption.REPLACE_EXISTING);
    }
}
