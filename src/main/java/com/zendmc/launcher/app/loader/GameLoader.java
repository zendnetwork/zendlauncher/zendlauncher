package com.zendmc.launcher.app.loader;

import com.zendmc.launcher.game.Downloader;
import com.zendmc.launcher.game.Launcher;
import org.greenrobot.eventbus.EventBus;

import java.io.IOException;

public class GameLoader {
    private static Downloader downloader;
    private static Launcher launcher;

    public static void start() throws IOException {
        downloader = new Downloader();
        launcher = new Launcher();

        EventBus.getDefault().register(downloader);
        EventBus.getDefault().register(launcher);
    }

    public static void stop() {
        EventBus.getDefault().unregister(downloader);
        EventBus.getDefault().unregister(launcher);
    }
}