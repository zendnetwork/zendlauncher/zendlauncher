package com.zendmc.launcher.app.loader;

import com.zendmc.launcher.app.App;
import com.zendmc.launcher.app.Migrater;
import com.zendmc.launcher.app.Updater;
import com.zendmc.launcher.util.HttpUtils;
import com.zendmc.launcher.util.SystemUtils;
import javafx.application.Platform;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

public class AppLoader {
    public static void start() throws IOException {
        List<String> p = App.getInstance().getParameters().getRaw();
        if (!p.contains("--no-updating")) {
            if (HttpUtils.isInternetConnected()) {
                Updater.checkUpdates();

                if (p.contains("--updated") && p.size() > 1) {
                    Migrater.migrate(p.get(p.size() - 1));
                }
            } else {
                Logger.getGlobal().warning("No Internet! NEITHER UPDATING NOR MIGRATING!");
            }
        }

        App.DEBUG = p.contains("--debug");

        Platform.setImplicitExit(App.SETTINGS.getJSONObject("launcher").getBoolean("closeLauncher"));

        JSONObject accountSettings = App.SETTINGS.getJSONObject("account");
        if (accountSettings.isNull("clientToken")) {
            accountSettings.put("clientToken", UUID.randomUUID().toString());
        }

        JSONObject gameSettings = App.SETTINGS.getJSONObject("game");
        if (gameSettings.isNull("directory")) {
            gameSettings.put("directory", SystemUtils.getDefaultGameDirectory());
        }
    }

    public static void stop() {}
}
