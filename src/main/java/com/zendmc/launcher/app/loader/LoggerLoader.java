package com.zendmc.launcher.app.loader;

import java.util.Date;
import java.util.logging.ConsoleHandler;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class LoggerLoader {
    public static void start() {
        ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new SimpleFormatter() {
            @Override
            public synchronized String format(LogRecord lr) {
                String format = "%1$tF %1$tT [%2$s] %3$s\n";
                String message;
                if (lr.getThrown() != null) {
                    StringBuilder stackTrace = new StringBuilder();
                    if (lr.getMessage() != null) {
                        stackTrace.append(lr.getMessage()).append("\n");
                    }
                    if (lr.getThrown().getMessage() != null) {
                        stackTrace.append(lr.getThrown().getMessage()).append("\n");
                    }

                    stackTrace.append(lr.getThrown().toString());

                    for (StackTraceElement ste : lr.getThrown().getStackTrace()) {
                        stackTrace.append("\n\t").append(ste);
                    }

                    for (Throwable t : lr.getThrown().getSuppressed()) {
                        stackTrace.append("\n\t").append(t);
                    }

                    message = stackTrace.toString();
                } else {
                    message = lr.getMessage();
                }
                return String.format(format, new Date(lr.getMillis()), lr.getLevel().getLocalizedName(), message);
            }
        });
        Logger.getGlobal().setUseParentHandlers(false);
        Logger.getGlobal().addHandler(handler);
    }

    public static void stop() {

    }
}
