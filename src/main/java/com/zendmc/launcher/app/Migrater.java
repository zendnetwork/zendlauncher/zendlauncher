package com.zendmc.launcher.app;

import com.zendmc.launcher.helper.JsonHelper;
import com.zendmc.launcher.util.FileUtils;
import com.zendmc.launcher.util.HttpUtils;
import org.json.JSONObject;

import java.io.IOException;

public class Migrater {
    public static void migrate(String oldHash) throws IOException {
        JSONObject migration = HttpUtils.downloadJsonObject(
                String.format(AppData.MIGRATION_URL, oldHash),
                FileUtils.createTempFile("-migration"),
                false,
                true
        );

        if (migration.has("added")) {
            JsonHelper.merge(App.SETTINGS, migration.getJSONObject("added"));
        }

        if (migration.has("removed")) {
            JsonHelper.remove(App.SETTINGS, migration.getJSONObject("removed"));
        }
    }
}
