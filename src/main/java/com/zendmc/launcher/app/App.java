package com.zendmc.launcher.app;

import com.zendmc.launcher.app.loader.AppLoader;
import com.zendmc.launcher.app.loader.GameLoader;
import com.zendmc.launcher.app.loader.GuiLoader;
import com.zendmc.launcher.app.loader.LoggerLoader;
import com.zendmc.launcher.app.loader.SettingsLoader;
import com.zendmc.launcher.event.PlatformExitEvent;
import javafx.application.Application;
import javafx.stage.Stage;
import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

public class App extends Application {

    public static JSONObject SETTINGS;
    public static boolean DEBUG = false;
    public static Stage STAGE;
    private static App instance;

    public static App getInstance() {
        return instance;
    }

    @Override
    public void start(Stage stage) {
        instance = this;
        STAGE = stage;

        try {
            LoggerLoader.start();
            SettingsLoader.start();
            AppLoader.start();
            GameLoader.start();
            GuiLoader.start();
        } catch (Exception ex) {
            Logger.getGlobal().log(Level.SEVERE, null, ex);
            System.exit(-1);
        }

        Logger.getGlobal().info("Successfully started");
    }

    @Override
    public void stop() {
        EventBus.getDefault().post(new PlatformExitEvent());
        try {
            GuiLoader.stop();
            GameLoader.stop();
            AppLoader.stop();
            SettingsLoader.stop();
            LoggerLoader.stop();
        } catch (Exception ex) {
            Logger.getGlobal().log(Level.SEVERE, null, ex);
            System.exit(-1);
        }

        Logger.getGlobal().info("Successfully stopped");
        System.exit(0);
    }
}