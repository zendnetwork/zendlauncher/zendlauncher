package com.zendmc.launcher.game;

public class GameData {
    public static final String VERSION_MANIFEST_URL = "https://meta.zendmc.com/minecraft/manifest.json";
    public static final String OBJECT_URL = "http://resources.download.minecraft.net/%s/%s";
    public static final String LIBRARY_URL = "https://libraries.minecraft.net/%s";
    public static final String AUTHENTICATION_URL = "https://authserver.mojang.com/authenticate";
    public static final String VALIDATION_URL = "https://authserver.mojang.com/validate";
    public static final String REFRESHMENT_URL = "https://authserver.mojang.com/refresh";

    public final static String ASSETS_PATH = "/assets";
    public final static String ASSETS_INDEX_PATH = "/assets/indexes/%s.json";
    public final static String LEGACY_OBJECTS_PATH = "/assets/virtual/legacy";
    public final static String OBJECT_PATH = "/assets/objects/%s/%s";
    public final static String LEGACY_OBJECT_PATH = "/assets/virtual/legacy/%s";
    public final static String LIBRARY_PATH = "/libraries/%s";
    public final static String VERSION_NATIVES_PATH = "/versions/%1$s-%2$s/natives";
    public final static String VERSION_META_PATH = "/versions/%1$s-%2$s/%2$s.json";
    public final static String VERSION_CLIENT_PATH = "/versions/%1$s-%2$s/%2$s.jar";
}
