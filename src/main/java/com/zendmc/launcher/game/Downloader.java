package com.zendmc.launcher.game;

import com.zendmc.launcher.app.App;
import com.zendmc.launcher.event.DownloadEvent;
import com.zendmc.launcher.event.LaunchEvent;
import com.zendmc.launcher.event.PlatformExitEvent;
import com.zendmc.launcher.helper.JsonHelper;
import com.zendmc.launcher.util.AlertUtils;
import com.zendmc.launcher.util.FileUtils;
import com.zendmc.launcher.util.HttpUtils;
import com.zendmc.launcher.util.SystemUtils;
import com.zendmc.launcher.util.Utf8Control;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Downloader {
    public static ArrayList<JSONObject> VERSIONS = new ArrayList<>();
    public static ArrayList<JSONObject> DOWNLOADED_VERSIONS = new ArrayList<>();
    private String gamePath;

    public Downloader() throws IOException {
        gamePath = App.SETTINGS.getJSONObject("game").getString("directory");

        byte[] pattern = "{\"online\":[],\"offline\":[]}".getBytes(StandardCharsets.UTF_8);
        Path path = Paths.get(gamePath + "/versions.json");
        FileUtils.createFile(path, false);
        if (path.toFile().length() == 0) {
            Files.write(path, pattern);
        }

        JSONObject manifest = new JSONObject(new String(Files.readAllBytes(path)));

        manifest.getJSONArray("offline").forEach(object -> DOWNLOADED_VERSIONS.add((JSONObject) object));

        if (HttpUtils.isInternetConnected()) {
            manifest.put("online", HttpUtils.downloadJsonArray(
                    GameData.VERSION_MANIFEST_URL,
                    FileUtils.createTempFile("-versions-list"),
                    true,
                    true
            ));
            manifest.getJSONArray("online").forEach(object -> VERSIONS.add((JSONObject) object));
        } else {
            manifest.put("online", new JSONArray());
            VERSIONS = DOWNLOADED_VERSIONS;
        }

        if (VERSIONS.isEmpty() && DOWNLOADED_VERSIONS.isEmpty()) {
            ResourceBundle bundle = ResourceBundle.getBundle(
                    String.format("lang/%s/messages", "downloader"),
                    new Locale(App.SETTINGS.getJSONObject("launcher").getString("language")),
                    new Utf8Control()
            );
            AlertUtils.simpleAlert(
                    Alert.AlertType.ERROR,
                    bundle.getString("alert.noVersions.header"),
                    bundle.getString("alert.noVersions.content")
            );
            Platform.exit();
        }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onPlatformExitEvent(PlatformExitEvent event) {
        try {
            Files.write(
                    Paths.get(gamePath + "/versions.json"),
                    new JSONObject()
                            .put("online",
                                    VERSIONS.isEmpty() ? new JSONArray() : new JSONArray(VERSIONS))
                            .put("offline",
                                    DOWNLOADED_VERSIONS.isEmpty() ? new JSONArray() : new JSONArray(DOWNLOADED_VERSIONS))
                            .toString()
                            .getBytes(StandardCharsets.UTF_8)
            );
        } catch (IOException e) {
            Logger.getGlobal().log(Level.SEVERE, null, e);
        }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onDownloadEvent(DownloadEvent event) {
        Logger.getGlobal().info("Downloading started");
        try {
            download(event.version);
        } catch (Exception ex) {
            Logger.getGlobal().log(Level.SEVERE, "Downloading failed due to unknown error", ex);
            return;
        }
        HttpUtils.getThreadPool().shutdown();
        try {
            HttpUtils.getThreadPool().awaitTermination(2, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
            Logger.getGlobal().log(Level.SEVERE, "Downloading failed due to the interruption or time out", ex);
            return;
        }
        Logger.getGlobal().info("Downloading succeed");

        EventBus.getDefault().post(new LaunchEvent(event.version));
    }

    private void download(JSONObject version) throws IOException {
        gamePath = App.SETTINGS.getJSONObject("game").getString("directory");
        if (HttpUtils.getThreadPool().isTerminated()) {
            HttpUtils.setThreadPool(HttpUtils.getNewThreadPool());
        }

        String metaPath = gamePath + String.format(
                GameData.VERSION_META_PATH,
                version.getString("type"),
                version.getString("id")
        );

        JSONObject meta = HttpUtils.downloadJsonObject(
                version.getString("url"),
                Paths.get(metaPath),
                false,
                true
        );

        if (meta.has("inheritsFrom") && !meta.has("merged")) {
            JSONObject inheritedMeta = HttpUtils.downloadJsonObject(
                    meta.getString("inheritsFrom"),
                    FileUtils.createTempFile("-raw-community-meta"),
                    true,
                    true
            );
            meta = JsonHelper.merge(inheritedMeta, meta);
            meta.put("merged", true);

            Files.delete(Paths.get(metaPath));
            Files.write(
                    Paths.get(metaPath),
                    meta.toString().getBytes(StandardCharsets.UTF_8)
            );
        }

        HttpUtils.downloadVoidAsync(
                meta.getString("client"),
                Paths.get(gamePath + String.format(
                        GameData.VERSION_CLIENT_PATH,
                        version.getString("type"),
                        version.getString("id")
                )),
                false
        );

        downloadAssets(meta);
        downloadLibraries(meta);
        downloadNatives(meta);
        DOWNLOADED_VERSIONS.add(version);
    }

    private void downloadAssets(JSONObject meta) {
        JSONObject assetsMeta = meta.getJSONObject("assets");
        JSONObject assets = HttpUtils.downloadJsonObject(
                assetsMeta.getString("url"),
                Paths.get(gamePath + String.format(GameData.ASSETS_INDEX_PATH, assetsMeta.getString("id"))),
                false,
                true
        ).getJSONObject("objects");

        for (String key : assets.keySet()) {
            String hash = assets.getJSONObject(key).getString("hash");
            String trimmedHash = hash.substring(0, 2);
            HttpUtils.downloadVoidAsync(
                    String.format(
                            GameData.OBJECT_URL,
                            trimmedHash,
                            hash
                    ),
                    Paths.get(
                            gamePath + (assetsMeta.getString("id").equals("legacy")
                                ? String.format(GameData.LEGACY_OBJECT_PATH, key)
                                : String.format(GameData.OBJECT_PATH, trimmedHash, hash)
                    )),
                    false
            );
        }
    }

    private void downloadLibraries(JSONObject meta) {
        meta.getJSONArray("libraries").forEach(object -> {
            String url;
            String path;

            if (object instanceof String) {
                path = (String) object;
                url = String.format(GameData.LIBRARY_URL, path);
            } else {
                JSONObject lib = (JSONObject) object;
                url = lib.getString("url");
                path = lib.getString("path");
            }

            HttpUtils.downloadVoidAsync(
                    url,
                    Paths.get(gamePath + String.format(GameData.LIBRARY_PATH, path)),
                    false
            );
        });
    }

    private void downloadNatives(JSONObject meta) {
        String nativeName = "natives-" + SystemUtils.getOSName();
        String nativeArchName = nativeName + "-" + SystemUtils.getOSArch();

        JSONObject all = meta.getJSONObject("natives");
        ArrayList<Object> selected = new ArrayList<>();
        if (all.has(nativeArchName)) {
            selected.addAll(all.getJSONArray(nativeArchName).toList());
        }
        if (all.has(nativeName)) {
            selected.addAll(all.getJSONArray(nativeName).toList());
        }

        selected.forEach(object -> {
            String path = (String) object;
            HttpUtils.downloadVoidAsync(
                    String.format(GameData.LIBRARY_URL, path),
                    Paths.get(gamePath + String.format(GameData.LIBRARY_PATH, path)),
                    false
            );
        });
    }
}
