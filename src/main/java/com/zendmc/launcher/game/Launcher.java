package com.zendmc.launcher.game;

import com.zendmc.launcher.app.App;
import com.zendmc.launcher.event.LaunchEvent;
import com.zendmc.launcher.helper.SceneHelper;
import com.zendmc.launcher.util.FileUtils;
import com.zendmc.launcher.util.SystemUtils;
import javafx.application.Platform;
import net.querz.nbt.CompoundTag;
import net.querz.nbt.ListTag;
import net.querz.nbt.NBTUtil;
import net.querz.nbt.StringTag;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Launcher {
    private String gamePath;

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onLaunchEvent(LaunchEvent event) {
        Logger.getGlobal().info("Launching");
        try {
            launch(event.version);
        } catch (IOException | InterruptedException ex) {
            Logger.getGlobal().log(Level.SEVERE, "Launching failed due to unknown error", ex);
        }
    }

    private void launch(JSONObject version) throws IOException, InterruptedException {
        gamePath = App.SETTINGS.getJSONObject("game").getString("directory");
        JSONObject meta = new JSONObject(new String(Files.readAllBytes(Paths.get(
                gamePath + String.format(
                        GameData.VERSION_META_PATH,
                        version.getString("type"),
                        version.getString("id")
                )
        ))));

        ArrayList<String> command = new ArrayList<>();

        command.add(SystemUtils.getJavaExecutable());

        unpackNatives(meta);
        constructCommand(meta, command);
        if (App.SETTINGS.getJSONObject("launcher").getBoolean("addZendToServers")) {
            addZendToServers();
        }

        Logger.getGlobal().info(command.toString());


        ProcessBuilder pb = new ProcessBuilder(command).directory(new File(gamePath));
        if (App.DEBUG) {
            pb.inheritIO();
        }

        Process p = pb.start();

        Platform.runLater(() -> App.STAGE.hide());
        p.waitFor();
        Platform.runLater(() -> {
            SceneHelper.reload("main");
            App.STAGE.setScene(SceneHelper.getScene("main"));
            App.STAGE.show();
        });
    }

    private void unpackNatives(JSONObject meta) {
        String nativeName = "natives-" + SystemUtils.getOSName();
        String nativeArchName = nativeName + "-" + SystemUtils.getOSArch();

        JSONObject all = meta.getJSONObject("natives");
        ArrayList<Object> selected = new ArrayList<>();
        if (all.has(nativeArchName)) {
            selected.addAll(all.getJSONArray(nativeArchName).toList());
        }
        if (all.has(nativeName)) {
            selected.addAll(all.getJSONArray(nativeName).toList());
        }

        String nativesPath = gamePath + String.format(
                GameData.VERSION_NATIVES_PATH,
                meta.getString("type"),
                meta.getString("id")
        );
        selected.forEach(object -> {
            String path = (String) object;
            FileUtils.unpackJarFile(nativesPath, gamePath + String.format(GameData.LIBRARY_PATH , path));
        });
    }

    private void constructCommand(JSONObject meta, ArrayList<String> command) {
        JSONObject account = App.SETTINGS.getJSONObject("account");
        JSONObject game = App.SETTINGS.getJSONObject("game");

        HashMap<String, Object> params = new HashMap<>();
        params.put("memory", (int) game.getDouble("memory"));
        params.put("natives_path", gamePath + String.format(
                GameData.VERSION_NATIVES_PATH,
                meta.getString("type"),
                meta.getString("id")
        ));
        params.put("classpath", getClasspath(meta));
        params.put("main_class", meta.getString("mainClass"));
        params.put("auth_player_name", account.getString("nickname"));
        params.put("version_name", meta.getString("id"));
        params.put("game_directory", gamePath);
        params.put("assets_root", gamePath + GameData.ASSETS_PATH);
        params.put("assets_index_name", meta.getJSONObject("assets").getString("id"));
        params.put("auth_uuid", account.getString("uuid"));
        params.put("auth_access_token", account.getString("accessToken"));
        params.put("user_properties", "{}");
        params.put("user_type", "legacy");
        params.put("version_type", meta.getString("type"));
        params.put("game_assets", gamePath + GameData.LEGACY_OBJECTS_PATH);
        params.put("auth_session", "token:" + account.getString("accessToken"));

        meta.getJSONArray("arguments").forEach(x -> {
            AtomicReference<String> arg = new AtomicReference<>(x.toString());

            ArrayList<String> markedToRemove = new ArrayList<>();
            params.forEach((key, value) -> {
                if (arg.get().contains(key)) {
                    arg.set(arg.get().replace("${" + key + "}", value.toString()));
                    markedToRemove.add(key);
                }
            });
            markedToRemove.forEach(params::remove);

            command.add(arg.get());
        });
    }

    private String getClasspath(JSONObject meta) {
        StringBuilder classpath = new StringBuilder();

        meta.getJSONArray("libraries").forEach(object ->
            classpath.append(gamePath).append(String.format(
                    GameData.LIBRARY_PATH,
                    object instanceof String ? (String) object : ((JSONObject) object).getString("path")
            )).append(";")
        );

        classpath.append(gamePath).append(String.format(
                GameData.VERSION_CLIENT_PATH,
                meta.getString("type"),
                meta.getString("id")
        ));

        return classpath.toString();
    }

    private void addZendToServers() throws IOException {
        File serversNbt = new File(gamePath + "/servers.dat");

        CompoundTag serverEntry = new CompoundTag();
        serverEntry.put("name", new StringTag("Zend Network"));
        serverEntry.put("ip", new StringTag("zendmc.com"));

        ListTag<CompoundTag> servers = new ListTag<>();
        servers.add(serverEntry);

        CompoundTag root;

        if (serversNbt.exists() && serversNbt.length() > 0) {
            root = (CompoundTag) NBTUtil.readTag(serversNbt);

            ListTag<?> list = (ListTag<?>) root.get("servers");
            if (!list.asCompoundTagList().contains(serverEntry)) {
                for (Object tag : list) {
                    servers.add((CompoundTag) tag);
                }

                root.put("servers", servers);
                NBTUtil.writeTag(root, serversNbt, false);
            }
        } else {
            FileUtils.createFile(serversNbt.toPath(), false);
            root = new CompoundTag();
            root.put("servers", servers);
            NBTUtil.writeTag(root, serversNbt, false);
        }
    }
}
