package com.zendmc.launcher.game;


import com.zendmc.launcher.app.App;
import org.json.JSONObject;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Authenticator {
    public static boolean authenticate(String username, String password) {
        JSONObject accountSettings = App.SETTINGS.getJSONObject("account");
        String accessToken = accountSettings.optString("accessToken");
        String clientToken = accountSettings.optString("clientToken");

        String result = null;
        try {
            if (accountSettings.getBoolean("sessionSaved")) {
                result = refresh(accessToken, clientToken);

                if (result != null && result.isEmpty()) {
                    result = validate(accessToken, clientToken);
                }
            } else {
                result = authenticate(username, password, clientToken);
            }
        } catch (IOException ex) {
            Logger.getGlobal().log(Level.SEVERE, null, ex);
        }

        if (result == null) {
            accountSettings.put("sessionSaved", false);
            return false;
        }

        JSONObject authResponse = new JSONObject(result);
        JSONObject selectedProfile = authResponse.getJSONObject("selectedProfile");
        accountSettings.put("nickname", selectedProfile.getString("name"));
        accountSettings.put("uuid", selectedProfile.getString("id"));
        accountSettings.put("accessToken", authResponse.getString("accessToken"));
        accountSettings.put("clientToken", authResponse.getString("clientToken"));
        accountSettings.put("sessionSaved", true);
        return true;
    }

    private static String authenticate(String username, String password, String clientToken) throws IOException {
        String content =  new JSONObject()
                .put("agent",
                        new JSONObject()
                                .put("name", "Minecraft")
                                .put("version", 1)
                )
                .put("username", username)
                .put("password", password)
                .put("clientToken", clientToken)
                .put("requestUser", true)
                .toString();
        return sendRequest(
                GameData.AUTHENTICATION_URL,
                content
        );
    }

    private static String validate(String accessToken, String clientToken) throws IOException {
        String content = new JSONObject()
                .put("accessToken", accessToken)
                .put("clientToken", clientToken)
                .toString();
        return sendRequest(
                GameData.VALIDATION_URL,
                content
        );
    }

    private static String refresh(String accessToken, String clientToken) throws IOException {
        String content = new JSONObject()
                .put("accessToken", accessToken)
                .put("clientToken", clientToken)
                .put("requestUser", true)
                .toString();
        return sendRequest(
                GameData.REFRESHMENT_URL,
                content
        );
    }

    private static String sendRequest(String url, String content) throws IOException {
        // Creates connection
        HttpsURLConnection connection = (HttpsURLConnection) new URL(url).openConnection();
        // Prepares client data
        // Enables client headers
        connection.setDoOutput(true);
        // Sends client headers
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Content-Length", String.valueOf(content.length()));
        // Sends client data
        connection.getOutputStream().write(content.getBytes(StandardCharsets.UTF_8));
        // Checks for returned data
        switch (connection.getResponseCode()) {
            case 200:
                BufferedReader responseStream = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String response = responseStream.readLine();
                responseStream.close();
                return response;
            case 204:
                return "";
            default:
                return null;
        }
    }
}
